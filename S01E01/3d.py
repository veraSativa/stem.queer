import numpy as np
import pandas as pd
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import random
from matplotlib import colors
data = []
for i in range(500):
    modal = random.randint(0,1)
    ds = .19
    
    if modal == 0:
        data.append(
            (random.normalvariate(.8, ds),
            random.normalvariate(.8, ds),
            random.normalvariate(.8, ds),
            None)
        )
    if modal == 1:
        data.append(
            (random.normalvariate(.2, ds),
            random.normalvariate(.2, ds),
            random.normalvariate(.2, ds),
            None)
        )

data3D = pd.DataFrame(data, columns=['x', 'y', 'z', 'label'])
data3D = data3D.append({'x': .7, 'y': .3, 'z': .2, 'label': 'Lela Camiona'}, ignore_index=True)
data3D = data3D.append({'x': .8, 'y': .8, 'z': .2, 'label': 'Lela Femme'}, ignore_index=True)
data3D = data3D.append({'x': .2 - ds, 'y': .2 - ds, 'z': .8 + ds, 'label': 'Leathermen'}, ignore_index=True)
data3D = data3D.append({'x': .25, 'y': .47, 'z': .5, 'label': 'Hombre bisexual andogíno'}, ignore_index=True)
data3D = data3D.append({'x': .2, 'y': .2, 'z': .2, 'label': 'Moda "Hombre"'}, ignore_index=True)
data3D = data3D.append({'x': .8, 'y': .8, 'z': .8, 'label': 'Moda "Mujer"'}, ignore_index=True)
data3D = data3D.append({'x': .2 + ds/2, 'y': .5, 'z': .8 + ds/2, 'label': 'Gay femenino'}, ignore_index=True)
data3D = data3D.append({'x': .2 - ds/2, 'y': .5, 'z': .2 - ds/2, 'label': 'Johnny Depp'}, ignore_index=True)

data3D = data3D.assign(temp=data3D.x + data3D.y + data3D.y)
norm = colors.Normalize(vmin=data3D.temp.min(), vmax=data3D.temp.max())

import matplotlib.pyplot as plt
#%matplotlib widget
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure(figsize=(16, 9))
ax = fig.add_subplot(111, projection='3d')
ax.scatter(data3D.x,
           data3D.y,
           data3D.z,
          c=data3D.temp,
          cmap='PiYG',
          norm=norm,
          edgecolors='black')
ax.set(xticks=[], yticks=[], zticks=[])
ax.set_xlabel('<-hombre [Sexos subconscientes] mujer->', fontsize=10)
ax.set_ylabel('<-masculina [Expresiones de género] femenina->', fontsize=8)
ax.set_zlabel('<-ginofilía [Orientación sexual/romantica] androfilía->', fontsize=8)
plt.title('las 3 inclinaciones intrísecas de género')

for i, row in data3D[data3D.label.isnull() == False].iterrows():
    #print(row)
    ax.text(row['x'], row['y'], row['z'], row['label'], zorder=1, size=10)
plt.show()